//
//  UITextField+Extensions.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 22/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import UIKit
import Combine

extension UITextField {
    // There are built-in publishers for key value observing.
    // Sadly the publisher for UITextField.text only fires when losing focus (i.e. on didEndEditing)
    // this will do the trick!
    var textPublisher: AnyPublisher<String, Never> {
        NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: self)
            .compactMap { $0.object as? UITextField }
            .map { $0.text ?? "" }
            .eraseToAnyPublisher()
    }
}
