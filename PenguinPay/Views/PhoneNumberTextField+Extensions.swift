//
//  PhoneNumberTextField+Extensions.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 22/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import UIKit
import PhoneNumberKit
import Combine

class PhoneNumberTextFieldKenya: PhoneNumberTextField {
    override var defaultRegion: String {
        get {
            return "KE"
        }
        set {} // exists for backward compatibility
    }
}

extension PhoneNumberTextField {
    private var phoneNumberTextFieldPublisher: AnyPublisher<PhoneNumberTextField, Never> {
        return NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: self)
            .compactMap { $0.object as? PhoneNumberTextField }
            .eraseToAnyPublisher()
    }
    
    var phoneNumberObjectPublisher: AnyPublisher<PhoneNumber, Never> {
        return phoneNumberTextFieldPublisher
//            .filter { $0.isValidNumber }
            .compactMap { try? $0.phoneNumberKit.parse($0.text ?? "") }
            .eraseToAnyPublisher()
    }
    
    var validPhoneNumberPublisher: AnyPublisher<Bool, Never> {
        return phoneNumberTextFieldPublisher
            .map { $0.isValidNumber }
            .eraseToAnyPublisher()
    }

    var phoneNumberPublisher: AnyPublisher<String, Never> {
        return phoneNumberTextFieldPublisher
            .map { $0.nationalNumber }
            .eraseToAnyPublisher()
    }

    var countryCodeNumberPublisher: AnyPublisher<String?, Never> {
        return phoneNumberTextFieldPublisher
            .map { $0.currentRegion }
            .removeDuplicates()
            .eraseToAnyPublisher()
    }
}
