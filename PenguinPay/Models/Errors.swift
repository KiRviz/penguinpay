//
//  Errors.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 22/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import Foundation

enum ConversionError: Error {
    case stringToDecimal
    case rateNotFound
}
