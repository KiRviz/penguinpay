//
//  CurrencyRates.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 22/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import Foundation

struct CurrencyRates: Codable {
    let rates: [String: Double]
    
    public func convert(amountUSD: Double, to: Currency) throws -> Double {
        guard let rate = rates[to.rawValue] else {
            throw ConversionError.rateNotFound
        }
        
        return rate * amountUSD
    }
}
