//
//  Currency.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 22/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import Foundation

enum Currency: String {
    case KES, NGN, TZS, UGX
    
    init?(countryCode: String?) {
        guard let countryCode = countryCode else {
            return nil
        }
        
        switch countryCode {
        case "KE": self = .KES
        case "NG": self = .NGN
        case "TZ": self = .TZS
        case "UG": self = .UGX
        default:
            return nil
        }
    }
}
