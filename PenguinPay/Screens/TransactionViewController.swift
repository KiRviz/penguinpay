//
//  TransactionViewController.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 07/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import UIKit
import Combine
import PhoneNumberKit

class TransactionViewController: UIViewController {
    @IBOutlet private var firstNameTextField: UITextField!
    @IBOutlet private var lastNameTextField: UITextField!
    @IBOutlet private var phoneNumberTextField: PhoneNumberTextField!
    @IBOutlet private var sendAmountTextField: UITextField!
    
    @IBOutlet private var firstNameHint: UILabel!
    @IBOutlet private var lastNameHint: UILabel!
    @IBOutlet private var phoneNumberHint: UILabel!
    @IBOutlet private var sendAmountHint: UILabel!
    @IBOutlet private var connectionHint: UILabel!
    
    @IBOutlet private var receiveAmountTextField: UITextField!
    @IBOutlet private var receiveCurrency: UILabel!
    @IBOutlet private var sendButton: UIButton!
    
    @IBAction func onSend(_ sender: Any) {
        viewModel.send()
    }
    
    private var viewModel: TransactionViewModel!
    
    private var bag = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneNumberTextField.withFlag = true
        phoneNumberTextField.withPrefix = true
        phoneNumberTextField.withExamplePlaceholder = true
        
        viewModel = TransactionViewModel(
            firstName: firstNameTextField.textPublisher,
            lastName: lastNameTextField.textPublisher,
            phoneNumber: phoneNumberTextField.textPublisher,
            sendAmountBinary: sendAmountTextField!.textPublisher,
            countryCode: phoneNumberTextField!.countryCodeNumberPublisher
        )
        
        bag = [
            viewModel.firstNameHintHidden.assign(to: \.isHidden, on: firstNameHint),
            viewModel.lastNameHintHidden.assign(to: \.isHidden, on: lastNameHint),
            viewModel.phoneNumberHintHidden.assign(to: \.isHidden, on: phoneNumberHint),
            viewModel.sendAmountHintHidden.assign(to: \.isHidden, on: sendAmountHint),
            viewModel.connectionHintHidden.assign(to: \.isHidden, on: connectionHint),
            viewModel.sendButtonEnabled.assign(to: \.isEnabled, on: sendButton),
            
            viewModel.receiveAmountBinary.assign(to: \.text, on: receiveAmountTextField),
            viewModel.receiveCurrencyCode.assign(to: \.text, on: receiveCurrency),
        ]
        
        setupNetworkErrorAlert()
        setupSuccessAlert()
    }
    
    private func setupNetworkErrorAlert() {
        let cancellable = viewModel.networkErrorMessage
            .sink { message in
                let alert = UIAlertController(title: "Oops", message: message, preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "OK", style: .default) { [weak self] alertAction in
                    self?.viewModel.setupRates()
                })

                self.present(alert, animated: true)
            }
        bag.insert(cancellable)
    }
    
    private func setupSuccessAlert() {
        let cancellable = viewModel.successMessage
            .sink { message in
                let alert = UIAlertController(title: "Woohoo 💥", message: message, preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

                self.present(alert, animated: true)
            }
        bag.insert(cancellable)
    }
}
