//
//  TransactionViewModel.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 21/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import Combine
import PhoneNumberKit

class TransactionViewModel {
    // input
    private let firstName = CurrentValueSubject<String, Never>("")
    private let lastName = CurrentValueSubject<String, Never>("")
    private let phoneNumber = PassthroughSubject<String, Never>()
    private let sendAmountBinary = CurrentValueSubject<String, Never>("")

    // output
    let firstNameHintHidden = CurrentValueSubject<Bool, Never>(true)
    let lastNameHintHidden = CurrentValueSubject<Bool, Never>(true)
    let phoneNumberHintHidden = CurrentValueSubject<Bool, Never>(true)
    let sendAmountHintHidden = CurrentValueSubject<Bool, Never>(true)
    let connectionHintHidden = CurrentValueSubject<Bool, Never>(true)
    let receiveAmountBinary = CurrentValueSubject<String?, Never>(nil)
    let receiveCurrencyCode = CurrentValueSubject<String?, Never>(nil)
    let sendButtonEnabled = CurrentValueSubject<Bool, Never>(false)
    let networkErrorMessage = PassthroughSubject<String?, Never>()
    let successMessage = PassthroughSubject<String?, Never>()

    // internal
    private let receiveCurrency = PassthroughSubject<Currency?, Never>()
    private var currencyRates = CurrentValueSubject<CurrencyRates?, Never>(nil)

    private let ratesClient = CurrencyRatesClient()
    private let phoneNumberKit = PhoneNumberKit()
    private let networkAvailability = NetworkAvialability()
    private lazy var isNetworkReachable = networkAvailability.availabilityPublisher
    
    private var bag = Set<AnyCancellable>()
    
    init(firstName: AnyPublisher<String, Never>,
         lastName: AnyPublisher<String, Never>,
         phoneNumber: AnyPublisher<String, Never>,
         sendAmountBinary: AnyPublisher<String, Never>,
         countryCode: AnyPublisher<String?, Never>) {
        
        let isFirstNameValid = firstName.map { $0.count > 0 }
        let isLastNameValid = lastName.map { $0.count > 0 }
        let isPhoneNumberValid = phoneNumber.map { [isNumberValid] in isNumberValid($0) }
        let isSendAmountValid = sendAmountBinary.map { Int($0, radix:2) ?? 0 > 0 }
        let isEverythingValid = Publishers.CombineLatest4(isFirstNameValid,
                                                          isLastNameValid,
                                                          isPhoneNumberValid,
                                                          isSendAmountValid)
            .map { $0 && $1 && $2 && $3 }
            .combineLatest(isNetworkReachable)
            .map { $0 && $1 }
        
        bag = [
            firstName.subscribe(self.firstName),
            lastName.subscribe(self.lastName),
            phoneNumber.subscribe(self.phoneNumber),
            sendAmountBinary.subscribe(self.sendAmountBinary),
            
            isFirstNameValid.subscribe(firstNameHintHidden),
            isLastNameValid.subscribe(lastNameHintHidden),
            isPhoneNumberValid.subscribe(phoneNumberHintHidden),
            isSendAmountValid.subscribe(sendAmountHintHidden),
            isNetworkReachable.subscribe(connectionHintHidden),
            isEverythingValid.subscribe(sendButtonEnabled),
            
            countryCode.map { Currency(countryCode: $0) }.subscribe(receiveCurrency),
            receiveCurrency.map { $0?.rawValue ?? "" }.subscribe(receiveCurrencyCode),
        ]
        
        setupRates()
        
        calculateReceiveAmount()
    }
    
    func send() {
        let message = "You sent \(sendAmountBinary.value) USD to \(firstName.value) \(lastName.value) and they received \(receiveAmountBinary.value ?? "") \(receiveCurrencyCode.value ?? "")"
        
        successMessage.send(message)
    }
    
    func setupRates() {
        let cancellable = isNetworkReachable.filter { $0 }.sink { [weak self] _ in
            // should normally be checking the latest rates every second
            // but since currently the app normally only checks once,
            // let's keep it once even when regaining connection
            guard self?.currencyRates.value == nil else {
                return
            }
            
            self?.ratesClient.getLatestRates() { result in
                switch result {
                case let .success(value):
                    self?.currencyRates.send(value)
                case let .failure(error):
                    self?.networkErrorMessage.send("Something went wrong, please check your internet connection and try again or get in touch with us prviding a screenshot with the message below\n\n\(error)")
                }
            }
        }
        bag.insert(cancellable)
    }
    
    private func calculateReceiveAmount() {
        let sendAmountDouble = sendAmountBinary.map { [weak self] in self?.binaryToDouble($0) }
        
        let receiveAmountDouble = Publishers.CombineLatest3(
            sendAmountDouble,
            receiveCurrency,
            currencyRates
            )
            .map { [weak self] (amount, currency, rates) -> Double? in
                self?.convert(amountUSD: amount, toCurrency: currency, rates: rates)
            }
            
        let receiveAmountBinary = receiveAmountDouble.map { [weak self] in self?.doubleToBinary($0) }
            
        bag.insert(
            receiveAmountBinary.subscribe(self.receiveAmountBinary)
        )
    }
    
    private func binaryToDouble(_ value: String) -> Double? {
        return Int(value, radix: 2).map { Double($0) }
    }
    
    private func doubleToBinary(_ value: Double?) -> String? {
        guard let value = value else {
            return nil
        }
        
        return String(Int(value), radix: 2)
    }
    
    private func convert(amountUSD: Double?, toCurrency: Currency?, rates: CurrencyRates?) -> Double? {
        guard let amount = amountUSD, let currency = toCurrency, let rates = rates else {
            return nil
        }
        
        return try? rates.convert(amountUSD: amount, to: currency)
    }
    
    private func isNumberValid(_ number: String) -> Bool {
        guard let phoneNumber = try? phoneNumberKit.parse(number) else {
            return false
        }
        
        // is country supported
        return Currency(countryCode: phoneNumber.regionID) != nil
    }
}
