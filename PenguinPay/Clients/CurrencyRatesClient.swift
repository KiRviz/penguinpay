//
//  CurrencyRatesClient.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 22/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import Foundation
import Alamofire
import Combine

class CurrencyRatesClient {
    private let baseURL = "https://openexchangerates.org/api"
    private let apiKey = "e74eac048572453593b1e73731cc5f61"
    private let baseCurrency = "USD"
    
    func getLatestRates(completion: @escaping (Result<CurrencyRates, AFError>) -> ()) {
        let (path, params) = Endpoints.latestRates(apiKey: apiKey,
                                                   baseCurrency: baseCurrency).endpoint
        
        AF.request(baseURL + path, parameters: params)
            .validate()
            .responseDecodable(of: CurrencyRates.self) { response in
                completion(response.result)
        }
    }
    
    enum Endpoints {
        case latestRates(apiKey: String, baseCurrency: String)
        
        var endpoint: (String, Parameters) {
            switch self {
            case let .latestRates(apiKey, baseCurrency):
                return ("/latest.json", ["app_id": apiKey, "base": baseCurrency])
            }
        }
    }
}
