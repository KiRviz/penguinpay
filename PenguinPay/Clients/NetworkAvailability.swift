//
//  NetworkAvailability.swift
//  PenguinPay
//
//  Created by Darius Jankauskas on 23/03/2020.
//  Copyright © 2020 Darius Jankauskas. All rights reserved.
//

import Foundation
import Network
import Combine

struct NetworkAvialability {
    let monitor = NWPathMonitor()
    
    var availabilityPublisher: AnyPublisher<Bool, Never> {
        let publisher = PassthroughSubject<Bool, Never>()
        
        let queue = DispatchQueue(label: "InternetConnectionMonitor")

        monitor.pathUpdateHandler = { pathUpdateHandler in
            DispatchQueue.main.async {
                if pathUpdateHandler.status == .satisfied {
                    publisher.send(true)
                } else {
                    publisher.send(false)
                }
            }
        }

        monitor.start(queue: queue)
        
        return publisher.eraseToAnyPublisher()
    }
}
