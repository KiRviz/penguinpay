# Penguin Pay

## Task
The task was to make a money transfer simulation from US to a few specified countries. Transfers are identified with the phone number. There's a twist that all user facing money numbers should be in binary form. So the app needs to convert form binary to decimal, then from USD to receiver's currency, then back to binary again.

## Notes

I kept the currency conversions in `Double` for this task for simplicity's sake. In a real world application dealing with money, would be good to use `Decimal`, even where it seems ok to use `Double`.

I used Combine AND Alamofire with callbacks in one project. Possibly a weird choice in a normal project but was great time-wise while still figuring out Combine.

The app handles network errors (in a very generic way) but doesn't handle very slow connection in a user friendly way (a big spinner would be nice). 

A few unit tests for the ViewModel would be nice (and easy) given a bit more time.
